#pragma once

#include "jarvis_perception/perception.hpp"


// ROS
#include <ros/ros.h>



#include<thread>


namespace jarvis_perception {

/*!
 * Main class for the node to handle the ROS interfacing.
 */
class System
{
 public:
  /*!
   * Constructor.
   * @param nodeHandle the ROS node handle.
   */
  System(ros::NodeHandle& nodeHandle);

  void SetImg(const sensor_msgs::ImageConstPtr& msg);
  void SetPointcloud(const sensor_msgs::PointCloud2& cloud);

  void SelectPointcloud(int x, int y);


  /*!
   * Destructor.
   */
  virtual ~System();

 private:
  //! ROS node handle.
  ros::NodeHandle& nodeHandle_;



  //! 
  Perception* perception_;


  /*!
   * Reads and verifies the ROS parameters.
   * @return true if successful.
   */
  bool readParameters();

};

} /* namespace */
