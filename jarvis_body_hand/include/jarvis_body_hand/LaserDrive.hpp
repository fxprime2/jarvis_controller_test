#pragma once
#include <ros/ros.h>
#include <tf/transform_broadcaster.h>
#include <tf/transform_listener.h>
#include <laser_geometry/laser_geometry.h>

#include <sensor_msgs/PointCloud2.h>
#include <std_msgs/Float64.h>
#include <dynamixel_msgs/JointState.h>

namespace jarvis_body_hand {

/*!
 * Class containing the algorithmic part of the package.
 */
class LaserDrive
{
 public:
  /*!
   * Constructor.
   */
  LaserDrive(ros::NodeHandle& nodeHandle, double period, double sweep);

  /*!
   * Destructor.
   */
  virtual ~LaserDrive();



 private:
  ros::NodeHandle& nodeHandle_;
  ros::Publisher pubDrive_,
                 pubCloud_;
  ros::Subscriber subDrive_,
                  subScan_;

  void DriveCallback(const dynamixel_msgs::JointState::ConstPtr &message);
  void ScanCallback(const sensor_msgs::LaserScan::ConstPtr& scan);
  tf::TransformListener tfListener_;
  double period_;
  double angle_;
  double sweep_;

  laser_geometry::LaserProjection projector_;

};

} /* namespace */
