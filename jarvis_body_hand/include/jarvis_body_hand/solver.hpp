#pragma once

namespace jarvis_body_hand {

/*!
 * Class containing the algorithmic part of the package.
 */
class Solver
{
 public:
  /*!
   * Constructor.
   */
  Solver();

  /*!
   * Destructor.
   */
  virtual ~Solver();

  /*!
   * Add new measurement data.
   * @param data the new data.
   */
  void addData(const double data);

  /*!
   * Get the computed average of the data.
   * @return the average of the data.
   */
  double getAverage() const;

 private:

  //! Internal variable to hold the current average.
  double average_;

  //! Number of measurements taken.
  unsigned int nMeasurements_;
};

} /* namespace */
