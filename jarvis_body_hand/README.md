# Jarvis low-level controller

## Overview

This package is used to low-level control for jarvis robot. This package is under construction and may lead some damage without guarantee. Use it wisely and be careful!

The jarvis low-level controller package has been tested under [ROS] Kinetic and Ubuntu 16.04. This is research code, expect that it changes often and any fitness for a particular purpose is disclaimed.

The source code is released under a [BSD 3-Clause license](jarvis_body_hand/LICENSE).

**Author(s): Thanabadee Burunseechart, paheyisoicus@gmail.com     
Maintainer: ABCD, EFG, .............,.............     
Affiliation: Smart Mechatronics Research Lab, Prince of songkha University**

![Grasping object](doc/fpv.png) 
![Grasping object](doc/rviz.png)


### Publications

If you use this work in an academic context, please cite the following publication(s):

* พิชยัพร บ่มไล่, อันวาร์ ราชาวนา, พฤทธิกร สมิตไมตรี*: การออกแบบและสมการอินเวอร์สคิเนเมติกส์ส าหรับแขนกลและมือจับของหุ่นยนต
์
ท างานภายในบ้าน.  RoboCup@Home, 2014. ([PDF](http://www.me.psu.ac.th/robocup/phocadownload/design%20of%20robot%20arm.pdf))

        @inproceedings{Fankhauser2014,
            author = {พิชยัพร บ่มไล่, อันวาร์ ราชาวนา, พฤทธิกร สมิตไมตรี},
            title = {{การออกแบบและสมการอินเวอร์สคิเนเมติกส์ส าหรับแขนกลและมือจับของหุ่นยนต์ท างานภายในบ้าน}},
            publisher = {robot@home},
            year = {2014}
        }






#### Dependencies

- [Robot Operating System (ROS)](http://wiki.ros.org) (middleware for robotics),
- [Eigen](http://eigen.tuxfamily.org/index.php?title=Main_Page) (linear algebra library)

		sudo apt-get install libeigen3-dev

- [Dynamixel](http://wiki.ros.org/dynamixel_controllers) (dynamixel control in python)
- [PCL](http://pointclouds.org/) (Pointcloud Library)
- [Moveit](http://moveit.ros.org/robots/) (Moveit)
- [darknet_ros](https://github.com/leggedrobotics/darknet_ros) (Darknet_ros)
- [cartographer_ros](https://github.com/googlecartographer/cartographer_ros) (Cartographer)

and other common tools in ros such as cv_bridge, tf_conversions, pcl_ros


## Installation
#### Building

To build from source, clone the latest version from this repository into your catkin workspace and compile the package using

	cd catkin_workspace/src
	git clone https://bitbucket.org/fxprime/jarvis_controller.git
	cd ../
	catkin_make


## Using All system

terminal $1

	roscore

terminal $2 (Moveit server)

	roslaunch jv_moveit_config demo.launch

terminal $3 (Mapping and active avoidance)

	roslaunch jarvis_body_hand mapping2d_movebase.launch

terminal $4 (Hardware controller and main system)
	
	roslaunch jarvis_body_hand all.launch

terminal $5 (Human following mode)
	
	roslaunch darknet_ros darknet_ros.launch





## Usage Record and playback
Launch motor manager

	roslaunch jarvis_body_hand controller_manager.launch

## Record and playback test
re-plug the dynamixel power to make sure all motors not solid

   	roslaunch jarvis_body_hand controller_manager.launch
	roscd jarvis_body_hand/bag_for_pose && rosbag record /motor_states/pan_tilt_port -O NAME_OF_POSE.bag

spawn motor controller

	roslaunch jarvis_body_hand start_arm_motor.launch

launch 

	roslaunch jarvis_body_hand rerun_arm.launch



## Joystick Input

	Mode selector -> select button

	Head tracking mode -> Logitech button

	Arrow -> move head

	Stick -> move base

	Button A -> Move arm to home position

	Button Y -> Move arm to prepare position


## Module

### Joyinput + ModeManager (control_input)

	/* Get Remote Control(RC) value that defined */
	controlin_->RC.axes[axe idx]
	controlin_->RC.buttons[button idx]

	/* Get drive mode */
	controlin_->GetDriveMode();

	/* Get head mode */
	controlin_->GetHeadMode();

### Playback (record_hand_position)

	/* Play bagfile that recorded in bag_for_pose folder */
	rec_hand_->LoadBag("bye bye");
    voice_->Talk("bye bye");

    /* Wait action (optional) */
    while(!rec_hand_->FINISH() && ros::ok()) {
      sleep(0.1);
    }


### Arm pick and place (kinetic)

	/* Movearm */
	kinetic_->MoveArmTo(Vector3d(x,y,z),
                        Vector3d(roll,pitch,yaw));
    /* Grasp sequence */
    kinetic_->GrapAt(Vector3d(x,y,z),
                     Vector3d(roll,pitch,yaw));

    /* Place sequence */
    kinetic_->PlaceAt(Vector3d(x,y,z),
                      Vector3d(roll,pitch,yaw));

### Navigation (drivebase)

	/* Define person name to track and follow */
	actuator_->setPersonOfInterest("person");

	/* Person following */
	actuator_->GetPersonGoal(tf::Vector3& persongoal_out);

	/* Person following is actived only in WP_MODE */

	/* Trick : We can define setPersonOfInterest to "any object" or "any table" which know about
	 * its transform to feed in GetPersonGoal to get persongoal_out. The persongoal_out 
	 * can later used as goal point for navigation system.
	 */

### Voice (voice)
	
	/* Text to speech */
	voice_->Talk("Hi i am jarvis");

### Perception (jarvis_perception node)

	/* room to implement many stuff about image processing in jarvis_perception */

### Head tracking (actuator)

	/* OBJECT -> object frame_id */
	actuator_->setObjectOfInterest(OBJECT);
	bool found = actuator_->LookingFor(OBJECT, Stransform);

	/* Person tracking from frame_id */
	actuator_->setPersonOfInterest("person");

## Nodes

### jarvis_body_hand
### jarvis_perception



#### Subscribed Topics

* **`/jarvis_perception/manipulator/object_point_split`** ([sensor_msgs/Temperature])

	The temperature measurements from which the average is computed.

* **`/joint_states`** ([sensor_msgs/Temperature])

	The temperature measurements from which the average is computed.


#### Published Topics

...



#### Parameters

* **`JOINT_GROUP`** (string array, default: 'right_hand')

	The arm groups that are corespond to rviz config.

* **`JOINT_START_IDX`** (int array, default: [0])

	The first index of each group. 

* **`JOINT_N`** (int, default: 5, min: 5, max: 10)

	The number of joints arm.

* **`JOINT_IK_DIR`** 

	The direction of each arm vs rviz.

* **`JOINT_RVIZ`** 

	The joint index alignment to rviz.

* **`JOINT_SCALE`** 

	The joint scale from encoder.

* **`JOINT_RAW_HOME`** 

	The joint raw data from the encoder.	

* **`HEAD_IDX`** 

	The head motor start index.	

* **`WHEEL_IDX`** 

	The wheel motor start index.	

* **`GRIP_OPEN`** 

	The angle (in rad) for open gripper.	

* **`GRIP_CLOSE`** 

	The angle (in rad) for close gripper.

* **`OBJECT`** 

	(For test) The object that we interest (to track/grip).	

* **`OBJECT_LAST_ID`** 

	The last id object that recognized.	

* **`OBJECT_NAME`** 

	The object name that available for track/grip.	

* **`OBJECT_ID`** 

	The object id of each object_name (1 object has many id/view).	

* **`HEAD_SEARCHING_SPEED`** 

	The neck turn rate for searching people.	

* **`CONTROL_HEAD_SENSE`** 

	The head control speed (by joystick).	

* **`RC_FORW_RATE`** 

	The drive rate forward.	

* **`RC_TURN_RATE`** 

	The turn rate by RC.	

* **`TURN_RATE_LIMIT`** 

	Maximum turn rate (used in both autonomous/manual).	

* **`FORW_LIMIT`** 

	Maximum velocity (used in both autonomous/manual).	

* **`Kv`** 

	P gain for linear velocity.	

* **`Kth`** 

	P gain for angular velocity.	




## Bugs & Feature Requests

Please report bugs and request features using the [Issue Tracker](https://bitbucket.org/fxprime/jarvis_controller/issues).
