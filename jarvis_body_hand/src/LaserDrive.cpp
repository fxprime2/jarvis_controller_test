#include "jarvis_body_hand/LaserDrive.hpp"

namespace jarvis_body_hand {

LaserDrive::LaserDrive(ros::NodeHandle& nodeHandle, double period, double sweep)
    : nodeHandle_(nodeHandle),
      period_(period),
      sweep_(sweep)
{
	subDrive_ = nodeHandle_.subscribe("/tilt_controller/state", 1,
                                      &LaserDrive::DriveCallback, this);
	subScan_  = nodeHandle_.subscribe("/scan", 1,
									  &LaserDrive::ScanCallback, this);
	pubDrive_ = nodeHandle_.advertise<std_msgs::Float64>
									 ("/tilt_controller/command", 10);

	pubCloud_ = nodeHandle_.advertise<sensor_msgs::PointCloud2> 
									 ("/sync_scan_cloud_filtered", 100, false);
}

LaserDrive::~LaserDrive()
{
}


void LaserDrive::DriveCallback(const dynamixel_msgs::JointState::ConstPtr &message)
{
	angle_ = message->current_pos;
	double t = ros::Time::now().toSec();
	const double PI = 3.141592653589793238463;
	const double theta = 2*PI/period_;
	double driveangle = 0.50*sweep_*sin(theta*t);
	pubDrive_.publish(driveangle);


	

}

void LaserDrive::ScanCallback(const sensor_msgs::LaserScan::ConstPtr& scan)
{
	static tf::TransformBroadcaster br;
  	tf::Transform transform;
  	tf::Quaternion q(0, -angle_, 0);
    transform.setRotation(q);

  	br.sendTransform(
  		tf::StampedTransform(
  			transform, ros::Time::now(), "camera", "laser"));
  	
	sensor_msgs::PointCloud2 cloud;
    tfListener_.waitForTransform("camera", "laser", scan->header.stamp, ros::Duration(10.0) );

    projector_.transformLaserScanToPointCloud("camera", *scan, cloud, tfListener_);
    pubCloud_.publish(cloud);
}


} /* namespace */
