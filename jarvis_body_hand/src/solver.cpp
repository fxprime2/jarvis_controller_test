#include "jarvis_body_hand/Solver.hpp"

namespace jarvis_body_hand {

Solver::Solver()
    : average_(0.0),
      nMeasurements_(0)
{
}

Solver::~Solver()
{
}

void Solver::addData(const double data)
{
  average_ = (nMeasurements_ * average_ + data) / (nMeasurements_ + 1);
  nMeasurements_++;
}

double Solver::getAverage() const
{
  return average_;
}

} /* namespace */
