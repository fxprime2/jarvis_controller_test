#include "jarvis_body_hand/perception.hpp"

namespace jarvis_body_hand {

Perception::Perception(ros::NodeHandle& nodeHandle,
					   std::string rgb_frame,
					   std::string robot_frame)
    : nodeHandle_(nodeHandle),
      rgb_frame_(rgb_frame),
      robot_frame_(robot_frame),
      inFrame(cvCreateImage(cvSize(640, 480), 8, 3)),
      cloud_pcl (new pcl::PointCloud<pcl::PointXYZRGB>)
{
	STOP_THREAD = false;
	REQUEST_RESET = true;
	PAUSED=false;

	readParameters();


	sub = nodeHandle_.subscribe("/camera/rgb/image_color", 20, &Perception::kinectCallBack, this);
	subDepth = nodeHandle_.subscribe("/camera/depth_registered/points",20,&Perception::depthCb, this);
	
	vector_pub = nodeHandle_.advertise<geometry_msgs::Vector3>("manipulator/object_point_split", 1000);
	vector_pub_pour = nodeHandle_.advertise<geometry_msgs::Vector3>("manipulator/pour", 1000);
	vector_pub_pointcloud = nodeHandle_.advertise<sensor_msgs::PointCloud2>("object_pointcloud", 1000);

    listener = new tf::TransformListener();
	printf("start click_objects\n");
	cvNamedWindow("IMG", 1 );
	cvSetMouseCallback("IMG", mouseCallback, this);



}




void Perception::Run() 
{
	// ros::spin();

	ros::AsyncSpinner spinner(4);
  	spinner.start();

	while(ros::ok())
	{
		//TODO check empty file , etc
		if( !STOP_THREAD ) 
		{
			//DO SOMETHING

			ROS_INFO("Perception_RUNNING_BITCH!");
			//Safe area to stop
			if(STOP_THREAD)
			{
				// break;
				while(STOP_THREAD && ros::ok()) {
					PAUSED = true;
					sleep(0.5);
				}
			}
			PAUSED=false;
		}else
		{
			PAUSED = true;
		}
		sleep(0.1);
	}
}

bool Perception::Stop() 
{
	STOP_THREAD=true;
	return PAUSED;
}

void Perception::RequestReset() 
{
	REQUEST_RESET = true;
	STOP_THREAD = false;
}

bool Perception::FINISH() const
{
	return true;//MotorListMsgQueue.size()==0;
}

void Perception::readParameters()
{
	// nodeHandle_.getParam("/JOINT_N", JOINT_N);
	// nodeHandle_.getParam("/JOINT_SCALE", JOINT_SCALE);
	// nodeHandle_.getParam("/JOINT_RAW_HOME", JOINT_RAW_HOME);
	// nodeHandle_.getParam("/JOINT_START_IDX", JOINT_START_IDX);
	// nodeHandle_.getParam("/JOINT_GROUP", JOINT_GROUP);
	// if(JOINT_RAW_HOME.size() != JOINT_SCALE.size()) 
	// {
	// 	ROS_ERROR("RECORD_HAND: Could not read parameters. Check size of parameters. Please use roslaunch to include param.");
 //    	ros::requestShutdown();
	// 	while(ros::ok());
	// }
}










Perception::~Perception()
{

}



void Perception::depthCb(const sensor_msgs::PointCloud2& cloud)
{
  pcl::PointCloud<pcl::PointXYZRGB>::Ptr cloud_tmp (new pcl::PointCloud<pcl::PointXYZRGB>);
  pcl::PCLPointCloud2 PCLPointCloud_tmp;
  if ((cloud.width * cloud.height) == 0)
    return; //return if the cloud is not dense!
  try {
    pcl_conversions::toPCL(cloud, PCLPointCloud_tmp);

    //pcl::fromROSMsg(cloud, *cloud_tmp);

    pcl::fromPCLPointCloud2(PCLPointCloud_tmp, *cloud_tmp);

    //cloud_tmp->header = cloud.header;

    timeStamp = cloud.header.stamp;
    listener->waitForTransform(rgb_frame_, cloud.header.frame_id, cloud.header.stamp, ros::Duration(1.0));
    pcl_ros::transformPointCloud(rgb_frame_, *cloud_tmp, *cloud_pcl, *listener);

  } catch (std::runtime_error e) {
    ROS_ERROR_STREAM("Error message: " << e.what());
  }
}

void Perception::mouseCallback(int event, int x, int y, int flags, void *param)
{
	//Magic trick to put Mouse event into class!!
	Perception *self = static_cast<Perception*>(param);
    self->doMouseCallback(event, x, y, flags);
}

void Perception::doMouseCallback(int event, int x, int y, int flags)
{
	if(event == CV_EVENT_LBUTTONUP) 
	{
		ROS_INFO("click_at : x:%d y:%d",x,y);
		geometry_msgs::Vector3 vector;
		vector.x = cloud_pcl->points[y*640+x].x;
		vector.y = cloud_pcl->points[y*640+x].y;
		vector.z = cloud_pcl->points[y*640+x].z;
		if( vector.x == vector.x 
			&& vector.y == vector.y
			&& vector.z == vector.z
		)
		{

            geometry_msgs::PointStamped kinect_point;
            geometry_msgs::PointStamped base_point;

            kinect_point.header.frame_id = rgb_frame_;
            kinect_point.header.stamp = timeStamp;
            kinect_point.point.x = vector.x;
            kinect_point.point.y = vector.y;
            kinect_point.point.z = vector.z;
            listener->transformPoint(robot_frame_,kinect_point,base_point);
            vector.x = base_point.point.x;
            vector.y = base_point.point.y;
            vector.z = base_point.point.z;

			//sensor_msgs::PointCloud2 cloud_tf_out;
			//pcl::toROSMsg(*cloud_pcl,cloud_tf_out);
            //vector_pub_pointcloud.publish(cloud_tf_out);
            vector_pub.publish(vector);
            std::cout << "Raw pts: x:" << kinect_point.point.x << " y : " << kinect_point.point.y << " z : " << kinect_point.point.z << std::endl; 
            printf("send : x:%.5f y:%.5f z:%.5f\n",vector.x,vector.y,vector.z);						
		}

	}
	if(event == CV_EVENT_RBUTTONUP) 
	{
        ROS_INFO("R_click");
		ROS_INFO("click_at : x:%d y:%d",x,y);
		geometry_msgs::Vector3 vector;
		vector.x = cloud_pcl->points[y*640+x].x;
		vector.y = cloud_pcl->points[y*640+x].y;
		vector.z = cloud_pcl->points[y*640+x].z;
		if( vector.x == vector.x 
			&& vector.y == vector.y
			&& vector.z == vector.z
		)
		{

            geometry_msgs::PointStamped kinect_point;
            geometry_msgs::PointStamped base_point;

            kinect_point.header.frame_id = rgb_frame_;
            kinect_point.header.stamp = timeStamp;
            kinect_point.point.x = vector.x;
            kinect_point.point.y = vector.y;
            kinect_point.point.z = vector.z;
            listener->transformPoint(robot_frame_,kinect_point,base_point);
            vector.x = base_point.point.x;
            vector.y = base_point.point.y;
            vector.z = base_point.point.z;

			//sensor_msgs::PointCloud2 cloud_tf_out;
			//pcl::toROSMsg(*cloud_pcl,cloud_tf_out);
            //vector_pub_pointcloud.publish(cloud_tf_out);
            vector_pub_pour.publish(vector);
            printf("send : x:%.5f y:%.5f z:%.5f\n",vector.x,vector.y,vector.z);						
		}

	}


	// Perception* settings = reinterpret_cast<Perception*>(param);
 //    settings->onMouse(event, x, y);
}

void Perception::kinectCallBack(const sensor_msgs::ImageConstPtr& msg)
{
	int inKey = 0;
	ROS_INFO("TEST");
	for(int i=0;i<640*480;i++)
	{
			inFrame->imageData[i*3] = msg->data[i*3];
			inFrame->imageData[i*3+1] = msg->data[i*3+1];
			inFrame->imageData[i*3+2] = msg->data[i*3+2];
	}
		
	cvShowImage("IMG",inFrame);

	inKey = cvWaitKey(1);
	if(inKey == 27){
		exit(0);
	}
}



} /* namespace */
